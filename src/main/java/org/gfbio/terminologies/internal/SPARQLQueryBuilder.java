
package org.gfbio.terminologies.internal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.environment.VirtGraphSingleton;
import org.gfbio.terminologies.exceptions.GFBioBadRequestException;
import org.gfbio.terminologies.exceptions.GFBioInternalException;
import org.gfbio.terminologies.exceptions.GFBioServiceUnavailableException;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Literal;
import com.hp.hpl.jena.rdf.model.RDFNode;

/**
 * @author Vincent Bohlen
 * The SPARQL queries are using namespaces that are defined in the virtuoso server. namely:
 * omv: http://omv.ontoware.org/ontology# and gfbio: http://terminologies.gfbio.org/terms/ontology#
 *
 */

public class SPARQLQueryBuilder {
    
    private static final String uriPrefix = StandardVariables.getUriprefix();
    private static final String metadata_graph = StandardVariables.getMetatadatagraph();
    private static final String metadataSchema = StandardVariables.getMetadaschema();
    private static final String skos = "http://terminologies.gfbio.org/terms/ontology#SKOS";
    private static final String owl = "http://omv.ontoware.org/2005/05/ontology#OWL";
    
    //Temporary names for the properties. May change later.
    private static final String classesNumber = "numberOfClasses";
    private static final String individualNumber = StandardVariables.getIndividualnumber();
    private static final String maximumNumberOfChildren = StandardVariables.getMaximumnumberofchildren();
    private static final String maximumDepth = StandardVariables.getMaximumdepth();
    private static final String twentyfiveChildren = StandardVariables.getTwentyfivechildren();
    private static final String averageNumberOfChildren = StandardVariables.getAveragenumberofchildren();
    private static final String propertyNumber = StandardVariables.getPropertynumber();
    private static final String singleChild = StandardVariables.getSinglechild();
    private static final String classesWithoutDefinition = StandardVariables.getClasseswithoutdefinition();
    private static final String numberOfleaves = StandardVariables.getNumberofleaves();
    private static final String classesWithoutLabel = StandardVariables.getClasseswithoutlabel();
    private static final String classesWithMoreThan1Parent = StandardVariables.getClasseswithmorethan1parent();
  
    public static String getSPARQLAllTerms(String terminology_id){       
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();       
        String metadata_query =
                       "SELECT ?lang ?graph ?label FROM <"+ uriPrefix + metadata_graph + ">"
                     + "WHERE {<"+ uriPrefix + hash + "> omv:hasOntologyLanguage ?lang."
                     + "<" + uriPrefix+ hash + "> gfbio:graph ?graph."
                     + "<" + uriPrefix+ hash + ">  gfbio:label ?label}";     
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode lang = null;
        RDFNode graph = null;
        String label = "";
        if (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            lang = result.get("lang");
            graph = result.get("graph");
            label = "<"+result.get("label").toString()+">";         
        }
        if(graph != null && lang != null) {
            
            query.append("SELECT DISTINCT ?uri ?label{");
            switch(lang.toString()){
                case (owl): 
                    if(label.equals("")){
                        label = "rdfs:label";
                    }
                    query.append("{SELECT ?uri ?label FROM <"+graph.toString()+"> ");
                    query.append("WHERE { ?uri a owl:Class . ?uri "+label+" ?label }}");
                    query.append(" UNION ");
                    query.append("{SELECT ?ind as ?uri ?label FROM <"+graph.toString()+"> ");
                    query.append("WHERE { ?uri a owl:Class . ?ind rdf:type ?uri . ?ind "+label+" ?label}}");
                    query.append("}");
                    break;
                case (skos):
                    if(label.equals("")){
                        label = "skos:prefLabel";
                    }
                    query.append("{SELECT ?uri ?label FROM <"+graph.toString()+"> ");
                    query.append("WHERE { ?uri a skos:Concept . ?uri "+label+" ?label }}");
                    query.append(" UNION ");
                    query.append("{SELECT ?ind as ?uri ?label FROM <"+graph.toString()+"> ");
                    query.append("WHERE { ?uri a skos:Concept . ?ind rdf:type ?uri . ?ind "+label+" ?label}}");
                    query.append("}");
                    break;
            }
            query.append(" order by LCASE (?label)");
        }
        return query.toString();
    }  
    public static String getSPARQLTerm(String terminology_id, String term_uri){        
        StringBuilder query = new StringBuilder();
        terminology_id = uriPrefix + terminology_id;   
            query.append( "SELECT ?attribute ?value "
                         + "FROM <"+terminology_id+"> "
                         +" WHERE {<" + term_uri + "> ?attribute ?value. FILTER ( ! isBLANK(?value))}");
        return query.toString();
    }   
    public static String getSPARQLAllBroader(String terminology_id, String term_uri){       
        StringBuilder query = new StringBuilder();
    	int hash = terminology_id.hashCode();       
        String metadata_query = 
                       "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadata_graph + ">"
                     + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                     + "<" + uriPrefix + hash + "> gfbio:graph ?graph."
                     + "<" + uriPrefix + hash + "> gfbio:label ?label}"; 
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode lang = null;
        RDFNode graph = null;
        RDFNode label = null;
        while (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            lang = result.get("lang");
            graph = result.get("graph");
            label = result.get("label");
        }
        if(graph != null && lang != null) {
        	String hierarchyQuery = "SELECT ?broaderUri ?narrowerUri from <"+graph+"> where {?broaderUri rdfs:subPropertyOf skos:broader . ?narrowerUri rdfs:subPropertyOf skos:narrower}";
            vqe = VirtuosoQueryExecutionFactory.create(hierarchyQuery, set);
            res = vqe.execSelect();
            RDFNode broaderUri = null;
            RDFNode narrowerUri = null;
            if(res.hasNext()){
                QuerySolution result = res.nextSolution();
                broaderUri = result.get("broaderUri");        
                narrowerUri = result.get("narrowerUri");
            }
            query.append("SELECT distinct ?broaderuri ?broaderlabel  FROM <"+graph+"> ");
            switch(lang.toString()){
            case(skos):
                query.append("WHERE {{"+
                			"SELECT distinct ?broaderuri ?broaderlabel FROM <"+graph+">"+
		                        "WHERE{"+
		                        "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
		                        "<" + term_uri + "> (<"+broaderUri+"> | skos:broader)+ ?broaderuri." +               
		                        "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
		                        "}} "+
                        "UNION "+
	                        "{SELECT distinct ?broaderuri ?broaderlabel FROM <"+graph+"> "+
		                        "WHERE{ "+
		                        "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
		                        "?broaderUri (<"+narrowerUri+"> | skos:narrower)+ <" + term_uri + ">." +               
		                        "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
		                        "}}}");
                break;
             case(owl):
                 query.append("WHERE {" +
                         "{SELECT distinct ?broaderuri ?broaderlabel FROM <"+graph+"> " +
                         "WHERE {"+  
                         "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." +
                         "<" + term_uri + "> rdfs:subClassOf+ ?broaderuri." +         
                         "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}"+
                         "UNION"+
                         "{SELECT distinct ?broaderuri ?broaderlabel FROM <"+graph+">"+ 
                         "WHERE {<"+term_uri+"> rdf:type ?type . ?type rdfs:subClassOf+ ?broaderuri." +         
                         "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}}");
                 break;
            }
        }
        return query.toString();
    }
    public static String getSPARQLAllNarrower(String terminology_id, String term_uri){       
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();       
        String metadata_query = 
                       "SELECT distinct ?lang ?graph ?label FROM <" + uriPrefix + metadata_graph + ">"
                     + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                     + "<" + uriPrefix + hash + "> gfbio:graph ?graph ."
                     + "<" + uriPrefix + hash + "> gfbio:label ?label}";       
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(metadata_query, set);
        ResultSet res = vqe.execSelect();      
        RDFNode lang = null;
        RDFNode graph = null;
        RDFNode label = null;
        while (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            lang = result.get("lang");
            graph = result.get("graph");
            label = result.get("label");
        }
        if(graph != null && lang != null) {
        	String hierarchyQuery = "SELECT ?broaderUri ?narrowerUri from <"+graph+"> where {?broaderUri rdfs:subPropertyOf skos:broader . ?narrowerUri rdfs:subPropertyOf skos:narrower}";
            vqe = VirtuosoQueryExecutionFactory.create(hierarchyQuery, set);
            res = vqe.execSelect();
            RDFNode broaderUri = null;
            RDFNode narrowerUri = null;
            if(res.hasNext()){
                QuerySolution result = res.nextSolution();
                broaderUri = result.get("broaderUri");        
                narrowerUri = result.get("narrowerUri");
            }
            query.append("SELECT distinct ?narroweruri ?narrowerlabel FROM <"+ graph.toString() +"> ");
            switch(lang.toString()){
                case(skos):
                    query.append("WHERE {{"+
                			"SELECT distinct ?narroweruri ?narrowerlabel FROM <"+graph+">"+
		                        "WHERE{"+
		                        "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
		                        "<" + term_uri + "> (<"+narrowerUri+"> | skos:narrower)+ ?narroweruri." +               
		                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
		                        "}} "+
                        "UNION "+
	                        "{SELECT distinct ?narroweruri ?narrowerlabel FROM <"+graph+"> "+
		                        "WHERE{ "+
		                        "?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
		                        "?narroweruri (<"+broaderUri+"> | skos:broader)+ <" + term_uri + ">." +               
		                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
		                        "}}}");
                    break;
                case(owl):
                    query.append("WHERE {" +
                        "{SELECT distinct ?narroweruri ?narrowerlabel FROM <"+graph+"> " +
                        "WHERE {"+  
                        "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." +
                        "?narroweruri rdfs:subClassOf+ <" + term_uri + ">." +         
                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}"+
                        "UNION"+
                        "{SELECT distinct ?narroweruri ?narrowerlabel FROM <"+graph+">"+ 
                        "WHERE {<"+term_uri+"> rdf:type ?type . ?narroweruri rdfs:subClassOf+ ?type." +         
                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}}");
                    break;
            }
        }
        return query.toString();
    }
    public static String getSPARQLBroader(String terminology_id, String term_uri){     
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();
        String metadata_query = 
                  "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadata_graph + ">"
                + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                + "<" + uriPrefix + hash + "> gfbio:graph ?graph ."
                + "<" + uriPrefix + hash + "> gfbio:label ?label}"; 
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect(); 
        RDFNode lang = null;
        RDFNode graph = null;
        RDFNode label = null;
        while (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            lang = result.get("lang");
            graph = result.get("graph");
            label = result.get("label");
        }
        if(graph != null && lang != null) {
            query.append("SELECT distinct ?broaderuri ?broaderlabel  FROM <"+ graph.toString() +"> ");
            switch(lang.toString()){
                case(skos):
                    query.append("WHERE {"+
                    		"{SELECT distinct ?broaderuri ?broaderlabel  FROM <"+ graph.toString() +"> " +
                            " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
                            "<" + term_uri + "> ?h ?broaderuri. ?h rdfs:subPropertyOf* skos:broader." +               
                            "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
                            "}}"+
                             "UNION"+
                             "{SELECT distinct ?broaderuri ?broaderlabel  FROM <"+ graph.toString() +"> "+
                            " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
                            "?broaderuri ?h <" + term_uri + "> . ?h rdfs:subPropertyOf* skos:narrower." +               
                            "?broaderuri ?p ?broaderlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
                            "}}}");
                    break;
                 case(owl):
                     query.append("WHERE {" +
                         "{SELECT distinct ?broaderuri ?broaderlabel FROM <http://terminologies.gfbio.org/terms/QUDT> " +
                         "WHERE {"+  
                         "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." +
                         "<" + term_uri + "> rdfs:subClassOf ?broaderuri." +         
                         "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}"+
                         "UNION"+
                         "{SELECT distinct ?broaderuri ?broaderlabel FROM <http://terminologies.gfbio.org/terms/QUDT>"+ 
                         "WHERE {<"+term_uri+"> rdf:type ?type . ?type rdfs:subClassOf ?broaderuri." +         
                         "?broaderuri ?p ?broaderlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}}");
                         break;
            }
        }
        return query.toString();
    }   
    public static String getSPARQLNarrower(String terminology_id, String term_uri){   
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();      
        String metadata_query = 
                  "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadata_graph + ">"
                + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                + "<" + uriPrefix + hash + "> gfbio:graph ?graph."
                + "<" + uriPrefix + hash + "> gfbio:label ?label}"; 
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode lang = null;
        RDFNode graph = null;
        RDFNode label = null;
        while (res.hasNext()) {
           QuerySolution result = res.nextSolution();
           lang = result.get("lang");
           graph = result.get("graph");
           label = result.get("label");
        } 
        if(graph != null && lang != null) {
            query.append("SELECT distinct ?narroweruri ?narrowerlabel FROM <"+ graph.toString() +"> ");
            switch(lang.toString()){
                case(skos):
                      query.append("WHERE{"+
                      "{SELECT distinct ?narroweruri ?narrowerlabel  FROM <"+ graph.toString() +"> " +
                      " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
                      "<" + term_uri + "> ?h ?narroweruri. ?h rdfs:subPropertyOf* skos:narrower." +               
                      "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
                      "}}"+
                       "UNION"+
                       "{SELECT distinct ?narroweruri ?narrowerlabel  FROM <"+ graph.toString() +"> "+
                      " WHERE{ ?graphuri a  <http://www.w3.org/2004/02/skos/core#ConceptScheme>."+
                      "?narroweruri ?h <" + term_uri + "> . ?h rdfs:subPropertyOf* skos:broader." +               
                      "?narroweruri ?p ?narrowerlabel. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
                      "}}}");
                    break;
                case(owl):
                    query.append("WHERE {" +
                        "{SELECT distinct ?narroweruri ?narrowerlabel FROM <http://terminologies.gfbio.org/terms/QUDT> " +
                        "WHERE {"+  
                        "?graphuri a  <http://www.w3.org/2002/07/owl#Ontology>." +
                        "?narroweruri rdfs:subClassOf <" + term_uri + ">." +         
                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}"+
                        "UNION"+
                        "{SELECT distinct ?narroweruri ?narrowerlabel FROM <http://terminologies.gfbio.org/terms/QUDT>"+ 
                        "WHERE {<"+term_uri+"> rdf:type ?type . ?narroweruri rdfs:subClassOf ?type." +         
                        "?narroweruri ?p ?narrowerlabel. FILTER(?p = rdfs:label || ?p = <"+label+">)}}}");
                break;
            }
        }
        System.out.println(query.toString());
        return query.toString();
    }
    public static String getSPARQLSynonym(String terminology_id, String term_uri){   
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();      
        String metadata_query = 
                  "SELECT  ?graph ?synonym FROM <" + uriPrefix + metadata_graph + ">"
                + "WHERE {<" + uriPrefix + hash + "> gfbio:graph ?graph ."
                    + "<" + uriPrefix + hash + "> gfbio:synonym ?synonym}";
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode graph = null;
        RDFNode synonym = null;
        if (res.hasNext()) {
           QuerySolution result = res.nextSolution();
           graph = result.get("graph");
           synonym = result.get("synonym");
        } 
        if(synonym != null){
	        query.append("SELECT ?synonym FROM <"+ graph.toString() +"> "
	            + "WHERE {<"+term_uri+"> ?p ?synonym. FILTER(?p = <"+synonym.toString()+">"
	                + "|| ?p = skos:altLabel)}");
	        }
        return query.toString();
    }
    public static String getSPARQLMetadata(String terminology_id){
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode(); 
        String metadata_query =
                  "SELECT ?lang ?graph FROM <" + uriPrefix + metadata_graph + ">"
                + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                + "<" + uriPrefix + hash + "> gfbio:graph ?graph}";
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode lang = null;
        RDFNode graph = null;
        while (res.hasNext()) {
           QuerySolution result = res.nextSolution();
           lang = result.get("lang");
           graph = result.get("graph");
        }
        if(graph != null && lang != null) {
            query.append("SELECT ?attribute ?value FROM <"+ graph.toString() +"> ");
            switch(lang.toString()){
                case(skos):
                    query.append("Where"
                            + "{{?uri a <http://www.w3.org/2004/02/skos/core#ConceptScheme> }"
                            + ".?uri ?attribute ?value. }");
                    break;
                case(owl):
                    query.append("Where {{ ?uri a <http://www.w3.org/2002/07/owl#Ontology> }"
                            + ".?uri ?attribute ?value. }");
                    break;
            }
        }
        return query.toString();
    }
    public static String getSPARQLTerminologies(){   
        StringBuilder query = new StringBuilder();
        query.append("SELECT ?acronym ?uri ?name ?description "
                        + "FROM <"+uriPrefix + metadata_graph + "> "
                        + "WHERE {?s a omv:Ontology . ?s omv:acronym ?acronym . "
                        + "?s gfbio:graph ?graph . ?s omv:URI ?uri . ?s omv:name ?name . ?s omv:description ?description}");
        return query.toString();
    }
    public static String getSPARQLMetrics(String terminology_id){   
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();    
        query.append("SELECT ?attribute ?value ?label FROM <"+uriPrefix+metadata_graph+">"
                        + " FROM <"+metadataSchema+"> "
                        + " FROM <http://omv.ontoware.org/2005/05/ontology> "
                        + "WHERE {<"+uriPrefix+hash+"> ?attribute ?value. "
                            + "FILTER (?attribute = omv:"+classesNumber
                            + "|| ?attribute = omv:"+individualNumber
                            + "|| ?attribute = omv:"+propertyNumber
                            + "|| ?attribute = gfbio:"+maximumDepth
                            + "|| ?attribute = gfbio:"+maximumNumberOfChildren
                            + "|| ?attribute = gfbio:"+averageNumberOfChildren
                            + "|| ?attribute = gfbio:"+twentyfiveChildren
                            + "|| ?attribute = gfbio:"+singleChild
                            + "|| ?attribute = gfbio:"+classesWithoutDefinition
                            + "|| ?attribute = gfbio:"+numberOfleaves
                            + "|| ?attribute = gfbio:"+classesWithMoreThan1Parent
                            + "|| ?attribute = gfbio:"+classesWithoutLabel
                            +")"
                        + " . OPTIONAL{?attribute rdfs:label ?label}}");
        return query.toString();
    }
    
    public static String getSPARQLTerminologyInfo(String terminology_id){
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();
        query.append("SELECT ?attribute ?value ?label FROM <"+uriPrefix + metadata_graph + "> "
                        + "FROM <"+metadataSchema+"> "
                        + "FROM <http://omv.ontoware.org/2005/05/ontology> "
                        + "WHERE {<"+uriPrefix+hash+"> ?attribute ?value. "
                            + "FILTER (!?attribute = omv:"+classesNumber
                            + "&& !?attribute = omv:"+individualNumber
                            + "&& !?attribute = omv:"+propertyNumber
                            + "&& !?attribute = gfbio:"+maximumDepth
                            + "&& !?attribute = gfbio:"+maximumNumberOfChildren
                            + "&& !?attribute = gfbio:"+averageNumberOfChildren
                            + "&& !?attribute = gfbio:"+twentyfiveChildren
                            + "&& !?attribute = gfbio:"+singleChild
                            + "&& !?attribute = gfbio:"+classesWithoutDefinition
                            + "&& !?attribute = gfbio:"+numberOfleaves
                            + "&& !?attribute = gfbio:"+classesWithMoreThan1Parent
                            + "&& !?attribute = gfbio:"+classesWithoutLabel
                            + "&& !?attribute = gfbio:graph"
                            + "&& !?attribute = gfbio:label"
                            + "&& !?attribute = gfbio:definition"
                            + "&& !?attribute = gfbio:synonym"
                            + "&& !?attribute = omv:resourceLocator"
                            + "&& !?attribute = rdf:type"
                            +")"
                        + " . OPTIONAL{?attribute rdfs:label ?label}}");
        return query.toString();
    }
 
	public static String getSPARQLSuggestSearch(String search_string, int limit) {
		search_string = search_string.replaceAll("\"", "");
		//search_string_mod = ".*" + search_string_mod + ".*";
	    String labelURIs = getSearchMetadata().get("labelURIs");
		StringBuilder query = new StringBuilder();
		query.append("select distinct ?uri, ?label, ?terminology, ?graph "
				+ "from <" + uriPrefix + metadata_graph + "> "
				+ "where { quad map virtrdf:DefaultQuadMap "
				+ "{ graph ?graph { ?uri ?labeluri ?label .");

		query.append(" FILTER ( (?labeluri = rdfs:label "+ labelURIs+") "
				+ "&& bif:contains(?label, '\"" + search_string + "*\"'))} . ");

		query.append("?o gfbio:graph ?graph. ?o omv:acronym ?terminology }}");
		query.append("order by  STRLEN(?label)");
		query.append("limit " + String.valueOf(limit));
		return query.toString();
	}
	
	public static String getSPARQLLabel(String terminology_id, String term_uri){       
        StringBuilder query = new StringBuilder();
        int hash = terminology_id.hashCode();       
        String metadata_query = 
                       "SELECT ?lang ?graph ?label FROM <" + uriPrefix + metadata_graph + ">"
                     + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
                     + "<" + uriPrefix + hash + "> gfbio:graph ?graph ."
                     + "<" + uriPrefix + hash + "> gfbio:label ?label}";   
        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
        ResultSet res = vqe.execSelect();
        RDFNode lang = null;
        RDFNode graph = null;
        RDFNode label = null;
        while (res.hasNext()) {
            QuerySolution result = res.nextSolution();
            lang = result.get("lang");
            graph = result.get("graph");
            label = result.get("label");
        }
        if(graph != null && lang != null) {
            query.append("SELECT ?label  FROM <"+graph+"> ");
            switch(lang.toString()){
                case (skos):
                    query.append("WHERE {" +
                            "<" + term_uri + "> ?p ?label. FILTER(?p = skos:prefLabel || ?p = <"+label+">)"+
                            "}");
                    break;
                case(owl):
                    query.append("WHERE {\n" +
                            "<" + term_uri + "> ?p ?label. FILTER(?p = rdfs:label || ?p = <"+label+">)"+
                            "}");
                    break;
            }
        }
        return query.toString();
    }
	   public static String getSPARQLType(String terminology_id, String term_uri){       
	        StringBuilder query = new StringBuilder();
	        int hash = terminology_id.hashCode();       
	        String metadata_query = 
	                       "SELECT ?lang ?graph  FROM <" + uriPrefix + metadata_graph + ">"
	                     + "WHERE {<" + uriPrefix + hash + "> omv:hasOntologyLanguage ?lang . "
	                     + "<" + uriPrefix + hash + "> gfbio:graph ?graph }";
	        VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
	        VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
	        ResultSet res = vqe.execSelect();
	        RDFNode lang = null;
	        RDFNode graph = null;
	        while (res.hasNext()) {
	            QuerySolution result = res.nextSolution();
	            lang = result.get("lang");
	            graph = result.get("graph");
	        }
	        if(graph != null && lang != null) {
	            query.append("SELECT ?type  FROM <"+graph+"> ");
	            query.append("WHERE {" +
	              "<" + term_uri + "> rdf:type ?type}");
	        }
	        return query.toString();
	    }
	public static String getSPARQLSearch(List<String> terminology_list, String match_type, String search_string) 
            throws GFBioBadRequestException, GFBioInternalException, 
            GFBioServiceUnavailableException {

	    String extensions = getSearchMetadata().get("extensions");
	    String labelURIs = getSearchMetadata().get("labelURIs");
        String search_string_mod = search_string.replaceAll("\"", "");
        StringBuilder query = new StringBuilder();
        query.append(
            "select distinct ?uri, ?terminology, ?graph, ?label "
                + "from <" + uriPrefix + metadata_graph + "> "
                + "where {{ quad map virtrdf:DefaultQuadMap "
                + "{ graph ?graph { ?uri ?labeluri ?label ."
            ); 

        if(match_type.equals("exact")) {    
            query.append(
                "?label bif:contains \"'" + search_string
                + "'\". FILTER ( STRLEN(?label) = "+ search_string_mod.length() + " && "
                + " ( ?labeluri = rdfs:label "+labelURIs.toString()+" ) ). "
                );
        } else if (match_type.equals("included")) {
            query.append(
                "?label bif:contains \"'"
                    + search_string
                    + "'\". FILTER ( ?labeluri = rdfs:label "+labelURIs.toString()+" ) . "
                );
        } else if (match_type.equals("regex")) {
            query.append(   
                " FILTER ( (?labeluri = rdfs:label "+labelURIs.toString()+" ) "
                    + "&& regex($label, \"'" + search_string_mod + "'\")) . "
                );
        }
        else{
            throw new GFBioBadRequestException();
        }
        if (!terminology_list.isEmpty()) {
            String terminologies = "";
            for (String term_items : terminology_list) {
                terminologies += " <" + uriPrefix + term_items + "> ";
            }
            query.append("values ?graph {" + terminologies + "}.");
        }
        query.append("}?o gfbio:graph ?graph. ?o omv:acronym ?terminology.}}");
        query.append(" UNION ");
        query.append("{quad map virtrdf:DefaultQuadMap "
                + "{ graph ?graph { ?uri ?p ?ext . ");
        if(match_type.equals("exact")) {    
            query.append(
                "?ext bif:contains \"'" + search_string
                + "'\". FILTER ( STRLEN(?ext) = "+ search_string_mod.trim().length() + " && "
                + " ( ?p = skos:altLabel "+extensions.toString()+" ) ). "
                );
        } else if (match_type.equals("included")) {
            query.append(
                "?ext bif:contains \"'"
                    + search_string
                    + "'\". FILTER ( ?p = skos:altLabel "+extensions.toString()+" ) . "
                );
        } else if (match_type.equals("regex")) {
            query.append(   
                " FILTER ( (?p = skos:altLabel "+extensions.toString()+" ) "
                    + "&& regex($ext, \"'" + search_string_mod + "'\")) . "
                );
        }
        else{
            throw new GFBioBadRequestException();
        }
        if (!terminology_list.isEmpty()) {
            String terminologies = "";
            for (String term_items : terminology_list) {
                terminologies += " <" + uriPrefix + term_items + "> ";
            }
            query.append("values ?graph {" + terminologies + "}.");
        }
        query.append(" ?uri ?labeluri ?label . FILTER (?labeluri = rdfs:label "+labelURIs.toString()+")");
        query.append("} ?o gfbio:graph ?graph. ?o omv:acronym ?terminology.}}}");
        System.out.println(query.toString());
        return query.toString();
    }
       
        private static HashMap<String, String> getSearchMetadata(){
            HashMap<String, String> map = new HashMap<String, String>();
            
            String metadata_query =
                "SELECT ?synonym ?abbreviation ?label ?symbol FROM <"+ uriPrefix + metadata_graph + ">"
              + "WHERE { ?graph gfbio:label ?label . OPTIONAL{?graph gfbio:synonym ?synonym} OPTIONAL{?graph gfbio:abbreviation ?abbreviation} . OPTIONAL {?graph gfbio:symbol ?symbol}}";  
            VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
            VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create (metadata_query, set);
            ResultSet res = vqe.execSelect();
            StringBuilder extensions = new StringBuilder();
            StringBuilder labelURIs = new StringBuilder();
            ArrayList<String> usedURIs = new ArrayList<String>();
            while (res.hasNext()) {
                QuerySolution result = res.nextSolution();
                if(result.get("synonym") != null && !usedURIs.contains(result.get("synonym").toString())
                            && !result.get("synonym").toString().equals("http://www.w3.org/2004/02/skos/core#altLabel")){
                    extensions.append(" || ");
                    extensions.append("?p = ");
                    extensions.append("<"+result.get("synonym")+">");
                    usedURIs.add(result.get("synonym").toString());
                }
                if(result.get("abbreviation") != null && !usedURIs.contains(result.get("abbreviation").toString())){
                    extensions.append(" || ");
                    extensions.append("?p = ");
                    extensions.append("<"+result.get("abbreviation")+">");
                    usedURIs.add(result.get("abbreviation").toString());
                }
                if(result.get("label") != null && !usedURIs.contains(result.get("label").toString()) 
                            && !result.get("label").toString().equals("http://www.w3.org/2000/01/rdf-schema#label")){
                    labelURIs.append(" || ");
                    labelURIs.append("?labeluri = ");
                    labelURIs.append("<"+result.get("label")+">");
                    usedURIs.add(result.get("label").toString());
                }
                if(result.get("symbol") != null && !usedURIs.contains(result.get("symbol").toString())){
                    extensions.append(" || ");
                    extensions.append("?p = ");
                    extensions.append("<"+result.get("symbol")+">");
                    usedURIs.add(result.get("symbol").toString());
                }
            }
            map.put("extensions", extensions.toString());
            map.put("labelURIs", labelURIs.toString());
            return map;
        }
}
