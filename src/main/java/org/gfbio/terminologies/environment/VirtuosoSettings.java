package org.gfbio.terminologies.environment;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class VirtuosoSettings {

	/*
	 * Use this code to store login data
	 	private Properties p;
		p.setProperty("username", "webservice");
		p.setProperty("hosturl", "jdbc:virtuoso://localhost:1111");
		p.setProperty("password", "BNnHp8euLDHNn4keey14ZRoBDJzDitXi");
		p.storeToXML(new FileOutputStream(propertyFile), "login data for Virtuoso data base");

	 */

	private String propertiFile = "VirtSettings.properties";
	private Properties p;

	public VirtuosoSettings(){
		p = new Properties();
		BufferedInputStream in;

		try {
			in = new BufferedInputStream(getClass().getClassLoader().getResourceAsStream(propertiFile));
			p.load(in);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getDBPassword(){
		return PasswordEncrypter.decryptPasswd(p.getProperty("password"));
	}
	public String getDBUserName(){
		return p.getProperty("username");
	}
	public String getHostURL(){		
		return p.getProperty("hosturl");
	}
}
