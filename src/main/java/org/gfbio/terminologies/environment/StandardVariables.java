package org.gfbio.terminologies.environment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Vincent Bohlen
 */
public class StandardVariables { 
    
    public enum TERMSERVICETYPES{original, processed, combined}
    
    //Graphs
    private static final String metadaSchema = "http://terminologies.gfbio.org/terms/ontology";
    private static final String uriPrefix = "http://terminologies.gfbio.org/terms/";
    private static final String metatadataGraph = "Metadata";
    private static final String tsSchema = "http://terminologies.gfbio.org/terms/ts-schema";
    private static final String omvURI = "http://omv.ontoware.org/2005/05/ontology#";
    //Metrics
    private static final String classesNumber = "numberOfClasses";
    private static final String individualNumber = "numberOfIndividuals";
    private static final String maximumNumberOfChildren = "maximumNumberOfChildren";
    private static final String maximumDepth = "maximumDepth";
    private static final String twentyfiveChildren = "classesWithMoreThan25Children";
    private static final String averageNumberOfChildren = "averageNumberOfChildren";
    private static final String propertyNumber = "numberOfProperties";
    private static final String singleChild = "classesWithASingleChild";
    private static final String classesWithoutDefinition = "classesWithoutDefinition";
    private static final String numberOfleaves = "numberOfLeaves";
    private static final String classesWithoutLabel = "classesWithoutLabel";
    private static final String classesWithMoreThan1Parent = "classesWithMoreThan1Parent";
    private static final String dtntaxonlistsWebserviceAcronym = "DTNtaxonlists_SNSB";
    //Other variables
    private static final String wsPrefix = "http://terminologies.gfbio.org/api";
    private static final ArrayList<String> standardLanguageAcronym = 
            new ArrayList<String>(Arrays.asList("en","eng"));
    private static final ArrayList<String> standardDescriptionAttributes = 
        new ArrayList<String>(Arrays.asList("dc:description")); 
    /*
     * SKOS AND OWL VALUES ARE DEFINED IN {@link SPARQLQueryBuilder} BECAUSE OF THE CASE 
     *    STATEMENT
     */
    
    
    
    //Getters
    public static String getMetadaschema() {
        return metadaSchema;
    }
    public static String getUriprefix() {
        return uriPrefix;
    }
    public static String getMetatadatagraph() {
        return metatadataGraph;
    }
    public static String getClassesnumber() {
        return classesNumber;
    }
    public static String getIndividualnumber() {
        return individualNumber;
    }
    public static String getMaximumnumberofchildren() {
        return maximumNumberOfChildren;
    }
    public static String getMaximumdepth() {
        return maximumDepth;
    }
    public static String getTwentyfivechildren() {
        return twentyfiveChildren;
    }
    public static String getAveragenumberofchildren() {
        return averageNumberOfChildren;
    }
    public static String getPropertynumber() {
        return propertyNumber;
    }
    public static String getSinglechild() {
        return singleChild;
    }
    public static String getClasseswithoutdefinition() {
        return classesWithoutDefinition;
    }
    public static String getNumberofleaves() {
        return numberOfleaves;
    }
    public static String getClasseswithoutlabel() {
        return classesWithoutLabel;
    }
    public static String getClasseswithmorethan1parent() {
        return classesWithMoreThan1Parent;
    }
    public static String getWsprefix() {
        return wsPrefix;
    }
    public static String getTSschema() {
        return tsSchema;
    }
    public static ArrayList<String> getStandardlanguageacronym() {
        return standardLanguageAcronym;
    }
    public static ArrayList<String> getStandarddescriptionattributes() {
        return standardDescriptionAttributes;
    }
    public static String getOmvuri() {
        return omvURI;
    }
    public static String getDtntaxonlistswebserviceacronym() {
        return dtntaxonlistsWebserviceAcronym;
    }
}
