package org.gfbio.terminologies.restws;

import java.util.ArrayList;
import java.util.List;

import javax.json.JsonObject;

import org.apache.log4j.Logger;
import org.gfbio.terminologies.external.ExternalWS;
/**
 * One service instance to encapsulate central needed objects
 * @author sbode <A HREF="mailto:sbode@marum.de">Steffen Bode</A>
 *
 */
public class SearchService {
	
	private static final Logger LOGGER = Logger.getLogger(SearchService.class);


	private ExternalWS externalWS;
	
	public SearchService() {
		this.externalWS = new ExternalWS();
		LOGGER.info(" search service created and ready ");
	
	}
	
	public boolean isContainsTerminology(String terminology_id){
		return this.externalWS.contains(terminology_id);
	}

	public List<JsonObject> createAndProcessSearchTasks(String search_string, String match_type, String terminologies_string, String first_hit, String internal_only, String filter) {
		Search searchTasks = new Search(
              search_string, 
              match_type, 
              terminologies_string, 
              first_hit,
              internal_only,
              filter, externalWS);
			return searchTasks.perform();
		
	}
	
}
