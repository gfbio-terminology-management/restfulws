package org.gfbio.terminologies.restws;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.gfbio.terminologies.external.ExternalWS;
import org.gfbio.terminologies.external.ParameterMap;
import org.gfbio.terminologies.internal.GFBioTS;
import org.gfbio.terminologies.internal.ParameterList;
/**
 * 
 * @author Vincent Bohlen
 *
 */
public class Search {
	
	
    private ParameterList terminology_list;
    private ParameterMap filterMap;
    private String terminologies_string;
    private String search_string;
    private String match_type;
    private String first_hit;
    private String internal_only;
    private String filter;
    private ExternalWS externalWS;

    private GFBioTS gfBioTS;
    //private String [] searchTokens; 

    
    /**
     * 
     * @param search_string
     * @param match_type
     * @param terminologies_string
     * @param first_hit
     * @param internal_only
     * @param externalWS
     */
    public Search(String search_string, String match_type,String terminologies_string, String first_hit,
                 String internal_only, String filter, ExternalWS externalWS){
        this.search_string = search_string;
        this.match_type = match_type;
        this.terminologies_string = terminologies_string;
        this.first_hit = first_hit;
        this.internal_only = internal_only;
        this.filter = filter;
        this.gfBioTS = GFBioTS.getInstance();
        this.externalWS = externalWS;
        
        
    }
    
//    private void tokenizeSearchString() {
//    	//FIXME clean harmonize String : comma, ;: ... 
//    	StringTokenizer sToken = new StringTokenizer(search_string);
//		this.searchTokens = new String[sToken.countTokens()];
//		
//	}
    
	/**
     * method to calculte the check value in the search function. uses a bin
     * represenatation and calculates the dec value.
     * @param list boolean describing if a terminology list specified
     * @param first boolean describing first_hit
     * @param internal boolean describing internal_only
     * @return dec value for the check
     */
    private int getParameterHash() { //FIXME can we avoid this complex way ? 
        int intlist = !terminology_list.isEmpty() ? 1 : 0;
        int intfirst = first_hit.equals("true") ? 1 : 0;
        int intinternal = internal_only.equals("true") ? 1 : 0;
        return intlist*4 + intfirst*2 + intinternal;
    }
    /**
     * method to perform the search on parameters which were specified in constructor
     * @return
     */
    public List<JsonObject> perform(){
        boolean paramValid = checkParameters();
        if(!paramValid){
            //TODO return error message
            //Maybe throw new GFBioBadRequestException()
            return null;
        }
        terminology_list = new ParameterList(this.terminologies_string);
        filterMap = new ParameterMap(this.filter);
        int check = getParameterHash();
        //String res = "";  //obsolete
        //Formatter formatter = new Formatter(format, query);
        List<JsonObject> dtoList = new ArrayList<JsonObject>();
        //List<String> errors; //obsolete
        //FIXME logger messages
        switch(check){        
        /* check   ||     list given  |     first_hit   | internal_only
         * ----------------------------------------------------
         *    0    ||        0        |        0        |        0
         * ----------------------------------------------------
         *    1    ||        0        |        0        |        1
         * ----------------------------------------------------
         *    2    ||        0        |        1        |        0
         * ----------------------------------------------------
         *    3    ||        0        |        1        |        1
         * ----------------------------------------------------
         *    4    ||        1        |        0        |        0
         * ----------------------------------------------------
         *    5    ||        1        |        0        |        1
         * ----------------------------------------------------
         *    6    ||        1        |        1        |        0
         * ----------------------------------------------------
         *    7    ||        1        |        1        |        1
         */
        
        case 0:
            dtoList = searchAll();
            break;
        case 1:
            dtoList = gfBioTS.search(search_string,match_type ,terminology_list);
            break;
        case 4:
            dtoList = searchAllList();
            break;
        case 5:
            dtoList = gfBioTS.search(search_string, match_type, terminology_list);
            break;
        case 6:
            dtoList = searchListFirst();
            break;
        case 7:
            dtoList = searchListFirst();
            break;
        }
        return dtoList;
    }
    /**
     * method to check if all given parameters have a valid format.
     * @return
     */
    private boolean checkParameters(){
        /*
         * RegEx to detect typing errors like:
         *  col
         *    COL,
         *    COL, BEFDATA
         *    COL,,BEFDATA
         *    "!������,������$",=)(,
         */
        if(!terminologies_string.toUpperCase().matches("(([A-Z_]+,)*([A-Z_]+))?")){
            return false;
        }
        if(!first_hit.matches("true|false")){
            return false;
        }
        if(!internal_only.matches("true|false")){
            return false;
        }
        return true;
    }
    /**
     * perform search on all internal and external terminologies //FIXME one of this method is obsolete ???
     * @return
     */
    private List<JsonObject> searchAll(){
        ArrayList<JsonObject> results = new ArrayList<JsonObject>();
        //Internal
        results.addAll(gfBioTS.search(search_string, match_type ,terminology_list));
        //External
        results.addAll(externalWS.search(search_string, match_type, terminology_list));
        return applyFilter(results);
    }    
    
    /**
     * perform search on all specified internal and external terminologies.
     * @return
     */
    private List<JsonObject> searchAllList(){
        ArrayList<JsonObject> results = new ArrayList<JsonObject>();        
        ParameterList pl = new ParameterList();
        for (String terminology : terminology_list) {
        	pl.add(terminology);
        }
        results.addAll(externalWS.search(search_string, match_type, pl));
        results.addAll(gfBioTS.search(search_string, match_type, pl));
        return applyFilter(results);
    }
    /**
     * perform search on all specified internal and external terminologies,
     * the search will break off after first positive search result.
     * @return
     */
    private List<JsonObject> searchListFirst(){

            ArrayList<JsonObject> results = new ArrayList<JsonObject>();
            for (String term : terminology_list) {
                if (externalWS.contains(term)) {
                    results.addAll(externalWS.search(search_string, match_type, term));
                    if (!results.isEmpty()){
                        break;
                    }
                }
                else{
                    results.addAll(gfBioTS.search(search_string, match_type, term));
                    if (!results.isEmpty()){
                        break;
                    }
                }
            }
            return applyFilter(results);
    }
    
    private ArrayList<JsonObject> applyFilter(ArrayList<JsonObject> results){
    	ArrayList<JsonObject> filtered = new ArrayList<JsonObject>();
    	for(JsonObject result : results){
        	boolean in = true;
    		for (Map.Entry<String, String> entry : filterMap.entrySet())
    		{
    			if(entry.getKey().equals("context")){
    				continue;
    			}
    			switch(entry.getValue()){
    			case "":
        			//case {'attribute':''}
    				if(result.containsKey(entry.getKey())){
    					try{
    						if(!result.getString(entry.getKey()).equals("")){
    							in = false;	
    						}
	    				//Easy distinguish between String and Array
	    				}catch(ClassCastException e){
	    					//Check each entry of the array
	    					for(JsonValue arrayPart : result.getJsonArray(entry.getKey())){
		        				if(!arrayPart.toString().equals("\"\"")){
		        					in = false;
		        					break;
		        				}
	    					}
	    				}
    				}
    				break;
    			default:
    				//case {'attribute':'something'}
    				if(!result.containsKey(entry.getKey())){
    					in = false;
    					break;
    				}
	    			try{
		    			if(!result.getString(entry.getKey()).contains(entry.getValue())){
		    				in = false;
		    			}
		    		//same as above
	    			}catch(ClassCastException e){
	    				for(String arrayPart : entry.getValue().split(",")){
		        			if(!result.getJsonArray(entry.getKey()).toString().contains(arrayPart.trim())){
		        				in = false;
		        				break;
		        			}
	    				}
	    			}
    				break;
    			}
    			//break for loop if one filter failed
    			if(!in){
    				break;
    			}
    		}
        	if(in){
        		filtered.add(result);
        	}
    	}
    	return filtered;  	
    }
}
