package org.gfbio.terminologies.restws;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;

import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.internal.ParameterList;
import org.json.CDL;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

/**
 * 
 * @author Alexandra La Fleur
 * @author Vincent Bohlen
 *
 */
public class Formatter {

	private List<JsonObject> resultList = new ArrayList<JsonObject>();
	private Set<String> contextProperties= new HashSet<String>();
	private JsonObjectBuilder prefixBuilder = Json.createObjectBuilder();
	private final  String schemaBase = StandardVariables.getTSschema()+"/";
	String query;
	String format;
	ParameterList terms;
	HashMap<String, String> contextEntries = new HashMap();

	public Formatter(String format, String query) {
		this.query = query;
		this.format = format;
	}

	public String formatResult(List<JsonObject> resultlist) {
	    if (resultlist != null) {
	        this.resultList.addAll(resultlist);
	    }
		Map<String, Object> config = new HashMap<String, Object>(1);
		config.put(JsonGenerator.PRETTY_PRINTING, true);
		JsonWriterFactory factory = Json.createWriterFactory(config);
		JsonArrayBuilder results_ab = Json.createArrayBuilder();
		JsonArrayBuilder diagnostics_ab = Json.createArrayBuilder();

		for (JsonObject result : resultList) {
			if (result != null && !result.isEmpty()) {
				if (result.containsKey("warning")
						|| result.containsKey("error")) {
					diagnostics_ab.add(result);
				} else if(result.containsKey("context")){
					//TODO find a better solution to get all the entries in context entries
					for(String pair : result.get("context").toString().split(",")){
						if(pair.equals("{}")){
							break;
						}
						String[] split = pair.split("\":\"");
						contextEntries.put(split[0].replace("{\"", "").replace("\"", ""), 
										split[1].replace("\"", "").replace("}", ""));
					}
				}
				else{
					results_ab.add(result);
				}
			}
		}
		JsonArray results = results_ab.build();
		JsonArray diagnostics = diagnostics_ab.build();

		JsonObject value = Json
				.createObjectBuilder()
				.add("request",
						Json.createObjectBuilder()
								.add("query", query)
								.add("executionTime",
										new java.util.Date().toString()))
				.add("results", results).add("diagnostics", diagnostics)
				.build();
		StringWriter sw = new StringWriter();
		JsonWriter jsonwriter = factory.createWriter(sw);
		jsonwriter.writeObject(value);
		switch (format) {
		case "xml":
			JsonObject value2 = Json.createObjectBuilder().add("root", value)
					.build();
			JSONObject json = new JSONObject(value2.toString());
			return XML.toString(json);
		case "csv":
			JSONArray res = new JSONObject(value.toString())
					.getJSONArray("results");
			String csv = CDL.toString(res);
			return csv;
		case "jsonld":
			JSONArray jsonResults = new JSONObject(value.toString())
			.getJSONArray("results");
			
			JsonArrayBuilder graphBuilder = buildGraph(jsonResults);
			
			JsonObjectBuilder contextBuilder = Json.createObjectBuilder();
			JsonObjectBuilder context = null;
			if(!contextEntries.isEmpty()){
				context = Json.createObjectBuilder();
			}
			for (Map.Entry<String, String> entry : contextEntries.entrySet())
			{
			    context.add(entry.getKey(), entry.getValue());
			}
			if(context == null){
			    contextBuilder.add("@context", prefixBuilder.build());
			}
			else{
			    contextBuilder.add("@context", context.build());
			}
			contextBuilder.add("@graph", graphBuilder.build());
			
			
			JsonObjectBuilder complete = Json.createObjectBuilder()
					.add("request",
							Json.createObjectBuilder()
									.add("query", query)
									.add("executionTime",
											new java.util.Date().toString()))
					.add("results",Json.createArrayBuilder().add(contextBuilder))
					.add("diagnostics", diagnostics);
			JsonObject completeObject = complete.build();
			StringWriter writer = new StringWriter();
			JsonWriter jwriter = factory.createWriter(writer);
			jwriter.writeObject(completeObject);
			return writer.toString();
		default:
			return sw.toString();
		}
	}

	private JsonArrayBuilder buildGraph(JSONArray array){
		JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
		for (int i = 0; i < array.length(); i++) {
			JsonObjectBuilder graphBuilder = Json.createObjectBuilder();
			JSONObject jsonObject = array.getJSONObject(i);
			Iterator iterator = jsonObject.keys();
			while(iterator.hasNext()){
				String key = (String) iterator.next();
				Object value = jsonObject.get(key);
				addDataToContextIfNotExists(key);
				if (value instanceof String){
					graphBuilder.add(key, (String) value);
				}
				else if (value instanceof JSONArray){
					JSONArray valueArray = (JSONArray) value;
					JsonArrayBuilder valueArrayBuilder = Json.createArrayBuilder();
					for (int j = 0; j<valueArray.length(); j++){
						valueArrayBuilder.add(valueArray.getString(j));
					}
					graphBuilder.add(key, valueArrayBuilder);
				}
			}
			arrayBuilder.add(graphBuilder.build());
		}
		return arrayBuilder;
	}
	
	private void addDataToContextIfNotExists(String key) {
		if(!contextEntries.containsKey(key)){
			contextEntries.put(key, schemaBase+key);
		}
	}	
}