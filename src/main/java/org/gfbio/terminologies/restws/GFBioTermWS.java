package org.gfbio.terminologies.restws;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletException;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.environment.StandardVariables.TERMSERVICETYPES;
import org.gfbio.terminologies.environment.VirtGraphSingleton;
import org.gfbio.terminologies.exceptions.GFBioBadRequestException;
import org.gfbio.terminologies.exceptions.GFBioServiceUnavailableException;
import org.gfbio.terminologies.external.ExternalWS;
import org.gfbio.terminologies.internal.GFBioTS;

import com.hp.hpl.jena.query.ResultSet;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import webservice.DWBWSserviceImpl;

/**
 * 
 * @author Alexandra La Fleur
 * @author Vincent Bohlen
 *
 */
@Path("terminologies")
public class GFBioTermWS{   
	
	private static final Logger LOGGER = Logger.getLogger(GFBioTermWS.class);


	//TODO consider static member inside a concurrent runtime
    private SearchService searchService;
    private ExternalWS externalWS = new ExternalWS();
    public GFBioTermWS() {
		this.searchService = new SearchService();
	}

    @GET
    @Path("")
    public Response getTerminologies(            
        @Context final UriInfo uriInfo,
        @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            resultList  = GFBioTS.getInstance().terminologies();
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
	@GET
    @Path("/{terminology_id}")
    public Response getTerminologyInfos(
                    @Context final UriInfo uriInfo,
                    @PathParam("terminology_id") String terminology_id, 
                    @QueryParam("format") @DefaultValue("json") String format) 
                                    throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external terminology info information.");
                resultList = externalWS.getTerminologyInfos(terminology_id);
            }
            else{
                LOGGER.info("Providing internal terminology info information.");
                resultList = GFBioTS.getInstance().terminologyInfos(terminology_id);
            }
            if(resultList.get(0).isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }

	@GET
    @Path("/{terminology_id}/metrics")
    public Response getMetrics(
                    @Context final UriInfo uriInfo,
                    @PathParam("terminology_id") String terminology_id, 
                    @QueryParam("format") @DefaultValue("json") String format) 
                                    throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external metrics information.");
                resultList = externalWS.getMetrics(terminology_id);
            }
            else{
                LOGGER.info("Providing internal metrics information.");
                resultList = GFBioTS.getInstance().metrics(terminology_id);
            }
            if(resultList.get(0).isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
    
    @GET
    @Path("/{terminology_id}/metadata")
    public Response getMetadata(
                    @Context final UriInfo uriInfo,
                    @PathParam("terminology_id") String terminology_id, 
                    @QueryParam("format") @DefaultValue("json") String format) 
                                    throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external metadata information.");
                resultList = externalWS.getMetadata(terminology_id);
            }
            else{
                LOGGER.info("Providing internal metadata information.");
                resultList  = GFBioTS.getInstance().metadata(terminology_id);
            }
            if(resultList.get(0).isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }

    @GET
    @Path("/{terminology_id}/allterms")
    public Response getAllTerms(
            @Context final UriInfo uriInfo,
            @PathParam("terminology_id") String terminology_id, 
            @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external allterms information.");
                resultList = externalWS.getAllTerms(terminology_id);
            }
            else{
                LOGGER.info("Providing internal allterms information.");
                resultList = GFBioTS.getInstance().allTerms(terminology_id);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
    
    @GET
    @Path("/{terminology_id}/term")
    public Response getTermInfos(
                    @Context UriInfo uriInfo,
                    @PathParam("terminology_id") final String terminology_id, 
                    @QueryParam("uri") @DefaultValue("")String term_uri,
                    @QueryParam("externalID") @DefaultValue("") String term_id, 
                    @QueryParam("format") @DefaultValue("json") String format, 
                    @QueryParam("output") @DefaultValue("combined") String output) 
                                    throws ServletException, IOException {  

        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            if(output.equals("processed") || output.equals("combined") || output.equals("original")){
                String termIdent = term_id.equals("")?term_uri:term_id;
                if(termIdent.equals("")){
                	return createError(format, formatter, 400);
                }
                if(this.externalWS.contains(terminology_id) 
                        || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                    LOGGER.info("Providing external term information.");
                    resultList = externalWS.getTermInfos(terminology_id, termIdent, output);
                }
                else{
                    LOGGER.info("Providing internal term information.");
                    resultList = (GFBioTS.getInstance().term(terminology_id, termIdent, output));
                }
                if(resultList.get(0).isEmpty()){
                	if(!isAvailable(terminology_id)){
                		return createError(format, formatter, 404);
                	}
                }
                res = formatter.formatResult(resultList);
            }
            else{
                LOGGER.info("Wrong output specified.");
                resultList = new ArrayList<JsonObject>();
                resultList.add(createWarning("The specified output format is not "
                    + "available. Please choose '"+TERMSERVICETYPES.combined.name()+"', "
                            + "'"+TERMSERVICETYPES.processed.name()+"' or "
                            + "'"+TERMSERVICETYPES.original.name()+"'."));
                res = formatter.formatResult(resultList);
            }
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }

	@GET
	@Path("/suggest")
	public Response suggest(@Context final UriInfo uriInfo,
			@QueryParam("query") String search_string,
			@QueryParam("limit") @DefaultValue("15") final String limit_str) {
		int limit;
		try {
			limit = Integer.parseInt(limit_str);
		} catch (NumberFormatException e) {
			limit = 15;
		}
		List<JsonObject> resultList = new ArrayList<JsonObject>();
		if(search_string.length() > 3){
		    resultList = GFBioTS.getInstance().suggestSearch(
		        search_string.trim(), limit);
		}
		else{
            resultList.add(createWarning("The service does not return results for"
                    + " queries shorter than four charaters."));
		}
		String calling_uri = uriInfo.getRequestUri().toString();
		int trunc_index = calling_uri.indexOf("/terminologies");
		Formatter formatter = new Formatter("json",
				"http://terminologies.gfbio.org/api/"
						+ calling_uri.substring(trunc_index));
        final String res = formatter.formatResult(resultList);
		return Response.ok(res)
				.header(HttpHeaders.CONTENT_TYPE, getFormat("json")).build();
	}

    @GET
    @Path("/search")
    public Response search(
            @Context final UriInfo uriInfo,
            @QueryParam("query") String search_string,
            @QueryParam("match_type") @DefaultValue("exact") final String match_type,
            @QueryParam("terminologies") @DefaultValue("") final String terminologies_string,
            @QueryParam("first_hit") @DefaultValue("false") final String first_hit,
            @QueryParam("format") @DefaultValue("json") String format,
            @QueryParam("internal_only") @DefaultValue("false") final String internal_only,
    		@QueryParam("filter") @DefaultValue("") final String filter)
    throws ServletException, IOException, GFBioBadRequestException,
            GFBioServiceUnavailableException {
        	LOGGER.info(" should process query " +search_string );
        	String noiseWordFilter = search_string;
        	noiseWordFilter = noiseWordFilter.replaceAll("[^A-Za-z0-9]", "");
        	List<JsonObject> resultList = new ArrayList<>();
        	if(!noiseWordFilter.equals("")){
                resultList = searchService.createAndProcessSearchTasks(search_string.trim(), 
                        match_type, 
                        terminologies_string, 
                        first_hit,
                        internal_only,
                        filter);
                LOGGER.info("SPARQL Query processed " + search_string );
        	}
        	else{
                resultList.add(createWarning("Search query consists of noise words only: "+search_string));
        	}
            if(!format.matches("json|xml|jsonld")){
                format = "json"; 
            }
            final String calling_uri = uriInfo.getRequestUri().toString();
            final int truncIndex = calling_uri.indexOf("/terminologies");
            Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
                    + calling_uri.substring(truncIndex));
            final String res = formatter.formatResult(resultList);
            LOGGER.info(" results formatted ... finish " );
            return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, GFBioTermWS.getFormat(format)).build();
    }

    @GET
    @Path("/{terminology_id}/narrower") 
    public Response getNarrower(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id, 
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external narrower information.");
                resultList = externalWS.getNarrower(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal narrower information.");
                resultList   = GFBioTS.getInstance().narrower(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
    @GET
    @Path("/{terminology_id}/synonyms") 
    public Response getSynonym(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id, 
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external synonym information.");
                resultList = externalWS.getSynonyms(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal synonym information.");
                resultList = GFBioTS.getInstance().synonym(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }

    @GET
    @Path("/{terminology_id}/broader") 
    public Response getBroader(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id,  
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external broader information.");
                resultList = externalWS.getBroader(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal broader information.");
                resultList = GFBioTS.getInstance().broader(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
    @GET
    @Path("/{terminology_id}/hierarchy") 
    public Response getHierarchy(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id,  
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external hierarchy information.");
                resultList = externalWS.getHierarchy(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal hierarchy information.");
                resultList = GFBioTS.getInstance().hierarchy(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);       
        }catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }
    
    @GET
    @Path("/{terminology_id}/allnarrower") 
    public Response getAllNarrower(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id, 
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external allnarrower information.");
                resultList = externalWS.getAllNarrower(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal allnarrower information.");
                resultList = GFBioTS.getInstance().allNarrower(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
           res = formatter.formatResult(resultList);
        }
        catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
    }

    @GET
    @Path("/{terminology_id}/allbroader") 
    public Response getAllBroader(
                @Context final UriInfo uriInfo,
                @PathParam("terminology_id") String terminology_id, 
                @QueryParam("uri") @DefaultValue("")String term_uri,
                @QueryParam("externalID") @DefaultValue("") String term_id, 
                @QueryParam("format") @DefaultValue("json") String format) 
                        throws ServletException, IOException {  
        final String calling_uri = uriInfo.getRequestUri().toString();
        final int truncIndex = calling_uri.indexOf("/terminologies");
        Formatter formatter = new Formatter(format, StandardVariables.getWsprefix() 
            + calling_uri.substring(truncIndex));
        List<JsonObject> resultList;
        String res;
        try{
            String termIdent = term_id.equals("")?term_uri:term_id;
            if(termIdent.equals("")){
            	return createError(format, formatter, 400);
            }
            if(this.externalWS.contains(terminology_id)
                    || terminology_id.contains(new DWBWSserviceImpl().getAcronym())){
                LOGGER.info("Providing external allbroader information.");
                resultList = externalWS.getAllBroader(terminology_id, termIdent);
            }
            else{
                LOGGER.info("Providing internal allbroader information.");
                resultList = GFBioTS.getInstance().allBroader(terminology_id, termIdent);
            }
            if(resultList.isEmpty()){
            	if(!isAvailable(terminology_id)){
            		return createError(format, formatter, 404);
            	}
            }
            res = formatter.formatResult(resultList);
        }
        catch(Exception e) {
        	Response r = handleException(e, format, formatter);
        	return r;
        }
        return Response.ok(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();        
    }
    
    /**
     * @param format: the string passed by the user as a queryParam. (xml,json,rdf,csv)
     * @return returns the corresponding MediaType for correct result type
     */
    private static String getFormat(String format) { //FIXME move to the formatter class
        String outputformat = MediaType.APPLICATION_JSON;
        if(format != null) {
            format = format.toLowerCase();
            switch(format){
            case "xml":
               outputformat = MediaType.APPLICATION_XML;
               break;
            case "json":
                outputformat = MediaType.APPLICATION_JSON;
                break;
            case "rdf":
                outputformat = MediaType.APPLICATION_XML;
                break;
            case "csv":
                outputformat = MediaType.TEXT_PLAIN;
                break;
            }
        }
        return outputformat;
    }
    private Response handleException(Exception e, String format, Formatter formatter) {
    	logException(e);
    	Response r;
    	if(e.getMessage() != null && e.getMessage().toLowerCase().contains("virtuoso")){
    		r = createError(format, formatter, 503);
    	}
    	else{
    		r = createError(format, formatter, 520);
    	}
        return r;
	}
    
    private JsonObject createWarning(String message){
    	return Json.createObjectBuilder()
    				.add("warning", message)
    				.build();
    }
    
    private Response createError(String format, Formatter formatter, int code) {
    	JsonObjectBuilder j = Json.createObjectBuilder();
    				if(code == 503){
    					j.add("error", "503 Service Unavailable");
    				}
    				else if(code == 520){
    					j.add("error", "520 Error Unknown: An unknown error occured. Please check your input.");
    				}
    				else if(code == 404){
    					j.add("error", "404 Not Found: The terminology you provided was not found.");
    				}
    				else if(code == 400){
    					j.add("error", "400 Bad Request: Parameter is missing.");
    				}
    	ArrayList<JsonObject> a = new ArrayList<JsonObject>();
    	a.add(j.build());
    	String res = formatter.formatResult(a);
    	return Response.status(code).entity(res).header(HttpHeaders.CONTENT_TYPE, getFormat(format)).build();
	}

	private void logException(Exception e) {
    	Writer writer = new StringWriter();
        PrintWriter printWriter = new PrintWriter(writer);
        e.printStackTrace(printWriter);
        LOGGER.error("Exception caught: " + writer.toString() + "printing generic error to user.");
	}
	private boolean isAvailable(String terminologyId) {
		String sparql = "select * from <"+StandardVariables.getUriprefix()+StandardVariables.getMetatadatagraph()+"> {?s omv:acronym ?o . ?o bif:contains '"+terminologyId+"'}";
		VirtGraph set = VirtGraphSingleton.getInstance().getVirtGraph();
		VirtuosoQueryExecution vqe = new VirtuosoQueryExecution(sparql, set);
		ResultSet rs = vqe.execSelect();
		if(rs.hasNext() || this.externalWS.contains(terminologyId)
        || terminologyId.contains(new DWBWSserviceImpl().getAcronym())){
			return true;	
		}
		return false;
	}
}
