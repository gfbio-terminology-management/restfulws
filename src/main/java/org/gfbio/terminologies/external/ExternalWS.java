package org.gfbio.terminologies.external;


import gov.usgs.itis.itis_service.ITISWSServiceImpl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.json.JsonObject;

import org.apache.log4j.Logger;
import org.gfbio.colwebservice.CatalogueOfLife;
import org.gfbio.interfaces.WSInterface;
import org.gfbio.resultset.ResultSet;
import org.gfbio.terminologies.environment.StandardVariables;
import org.gfbio.terminologies.environment.StandardVariables.TERMSERVICETYPES;
import org.gfbio.terminologies.restws.WSTask;
import org.gfbio.terminologies.restws.WSTasksHandling;

import webservice.DWBWSserviceImpl;
import PESI.v0_5.PESIWSServiceImpl;
import de.externalwebservices.worms.aphiav1_0.WORMSWSServiceIMPL;
import de.geonames.webservice.GeonamesWSServiceImpl;


/**
 * This class is a register for external web services like the Catalogue of life, WoRMS, etc.
 * A HashMap is used to store the instances of those services
 * @author Vincent Bohlen
 */
public class ExternalWS {
	
	private static final Logger LOGGER = Logger.getLogger(WSTasksHandling.class);

    private HashMap<String,WSInterface> WSList;
    
    private final static WSTasksHandling wsTasksHandling = WSTasksHandling.getWstasksInstance();;
    
    
    public ExternalWS(){
    	
        // Add all external web services here, 
    	/*
    	 * FIXME create a lifesign and remover watcher (TimerTask)
    	 * this means a static scheduled MINI threadpool or Task for the whole servlet runtime
    	 * liek the ITIS case of 20. november 2014
    	 */
    	//with OSGi we can here code a live admin function for insert and remove webservices
        WSList = new HashMap<String,WSInterface> ();
        WSList.put(CatalogueOfLife.ACRONYM, new CatalogueOfLife());
        WSInterface worms = new WORMSWSServiceIMPL();
        WSList.put(worms.getAcronym(), worms);
        WSInterface itis = new ITISWSServiceImpl();
        WSList.put(itis.getAcronym(), itis);
        WSInterface pesi = new PESIWSServiceImpl();
        WSList.put(pesi.getAcronym(), pesi);
        WSInterface geonames = new GeonamesWSServiceImpl();
        WSList.put(geonames.getAcronym(),geonames);
        WSInterface dwb = new DWBWSserviceImpl();
        WSList.put(new DWBWSserviceImpl().getAcronym(), dwb);
        
        LOGGER.info("External webservices ready for use");
    }
    

    public boolean contains(String acronym){
        return WSList.containsKey(acronym);
    }
    
    public HashMap<String, WSInterface> getWsList(){
        return WSList;
    }
    
    /**
     * FIXME write me
     * @param terminologyID
     * @param termUri
     * @return
     */
    public List<JsonObject> getAllBroader
            (final String terminologyID, final String term_uriORexternalID){
        WSInterface ws;
        if(terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())){
            String[] att = terminologyID.split("-");
            ws = new DWBWSserviceImpl(att[1], att[2]);
        }
        else{
            ws = WSList.get(terminologyID);
        }
        return ws.getAllBroader(term_uriORexternalID).create();
    }
    public List<JsonObject> getHierarchy
            (final String terminologyID, final String term_uriORexternalID){
        WSInterface ws;
        if(terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())){
            String[] att = terminologyID.split("-");
            ws = new DWBWSserviceImpl(att[1], att[2]);
        }
        else{
            ws = WSList.get(terminologyID);
        }
        return ws.getHierarchy(term_uriORexternalID).create();
    }
    
    public List<JsonObject> getBroader(final String terminologyID, final String term_uriORexternalID){
        ResultSet rs = new ResultSet("internal");
        rs.addWarning("The requested service is not available for the given "
            + "terminology");
        return rs.create();
    }
    public List<JsonObject> getNarrower(final String terminologyID, final String term_uriORexternalID){
        ResultSet rs = new ResultSet("internal");
        rs.addWarning("The requested service is not available for the given "
            + "terminology");
        return rs.create();
    }
    public List<JsonObject> getAllNarrower(final String terminologyID, final String term_uriORexternalID){
        ResultSet rs = new ResultSet("internal");
        rs.addWarning("The requested service is not available for the given "
            + "terminology");
        return rs.create();
    }

    public List<JsonObject> getMetrics(final String terminologyID){
        ResultSet rs = new ResultSet("internal");
        rs.addWarning("The requested service is not available for the given "
            + "terminology");
        return rs.create();
    }
    public List<JsonObject> getMetadata(final String terminologyID){
            ResultSet rs = new ResultSet("internal");
            rs.addWarning("The requested service is not available for the given "
                + "terminology");
            return rs.create();
    }
    public List<JsonObject> getAllTerms(final String terminologyID){
            ResultSet rs = new ResultSet("internal");
            rs.addWarning("The requested service is not available for the given "
                + "terminology");
            return rs.create();
    }
    public List<JsonObject> getTermInfos(final String terminologyID, 
                final String term_uriORexternalID, final String termServiceType){
        WSInterface ws;
        if(terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())){
            String[] att = terminologyID.split("-");
            ws = new DWBWSserviceImpl(att[1]);
        }
        else{
            ws = WSList.get(terminologyID);
        }
        if(termServiceType.toLowerCase().equals(TERMSERVICETYPES.original.name())){
            return ws.getTermInfosOriginal(term_uriORexternalID).create();
        }
        else if(termServiceType.toLowerCase().equals(TERMSERVICETYPES.processed.name())){
            return ws.getTermInfosProcessed(term_uriORexternalID).create();
        }
        else if(termServiceType.toLowerCase().equals(TERMSERVICETYPES.combined.name())){
            return ws.getTermInfosCombined(term_uriORexternalID).create();
        }
        ResultSet rs = new ResultSet("internal");
        rs.addWarning("The term service type"+termServiceType+"is unknown. Please specify"
        +TERMSERVICETYPES.original.name()+", "+TERMSERVICETYPES.processed.name()+" or "
            +TERMSERVICETYPES.combined.name());
        return rs.create();
    }
    public List<JsonObject> getSynonyms(final String terminologyID, final String term_uriORexternalID){
        if(terminologyID.equals("COL")){
            ResultSet rs = new ResultSet("internal");
            rs.addWarning("The requested service is not available for the given "
                + "terminology");
            return rs.create();
        }
        WSInterface ws;
        if(terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())){
            String[] att = terminologyID.split("-");
            ws = new DWBWSserviceImpl(att[1], att[2]);
        }
        else{
            ws = WSList.get(terminologyID);
        }
        return ws.getSynonyms(term_uriORexternalID).create();
    }
    public List<JsonObject> getTerminologyInfos(final String terminologyID){
        WSInterface ws;
    	if(terminologyID.contains(StandardVariables.getDtntaxonlistswebserviceacronym())){
            String[] att = terminologyID.split("-");
            if(att.length==2){
            	ws = new DWBWSserviceImpl(att[1]);
            }
            else{
            	ws = new DWBWSserviceImpl();
            }
        }
        else{
            ws = WSList.get(terminologyID);
        }
        return ws.getMetadata().create();
    }

    /**
     * Searches all registered external terminologies
     * @param searchString
     * @param matchType
     * @param terminologyList
     * @return
     */
    public List<JsonObject> search(final String searchString, final String matchType, final List<String> terminologyList){
        List <JsonObject> result = Collections.synchronizedList(new ArrayList<JsonObject>());
        List <Future<List<JsonObject>>> externalWSTasks = new ArrayList<>();

        if (terminologyList.isEmpty()) terminologyList.addAll(WSList.keySet()) ;
        
        for(String terminology : terminologyList){
            WSInterface ws = WSList.get(terminology);
            if (ws != null && ws.supportsMatchType(matchType) && ws.isResponding()) {
                
                // Thread creations put the threads to the threadpool
                final WSTask wsTask = new WSTask(ws, searchString, matchType);
                final Future<List<JsonObject>> wsTaskLink = wsTasksHandling.submitTask(wsTask);
                externalWSTasks.add(wsTaskLink);
                LOGGER.info("external webservice task as a thread started " 
                        +ws.getAcronym());
            }
        }
        LOGGER.info("external webservice tasks are running totally " +externalWSTasks.size());
        //await the finish of all created threads and 
        
        for(Future<List<JsonObject>> wsTaskLink : externalWSTasks ) {
        	
        	/*tricky way, this call blockes until THIS thread task has been finished
        	 * so the longest execution of a single thread task is the whole processtime for all webservice calls!
        	 */
        	
            try {
            	final List<JsonObject> threadResult = wsTaskLink.get(); 
            	//if done this adressed thread gives us his result and we add this to the central resultset
            	if(threadResult != null) {
            		result.addAll(threadResult);
            	} else{
            		LOGGER.info("thread returns null ");
            	}
                LOGGER.info("a external webservice task is done ");
            } catch (InterruptedException e) {
                LOGGER.error(e);
                e.printStackTrace();
            } catch (ExecutionException e) {
                LOGGER.error(e);
            }     	
        }
        LOGGER.info("all asynchronous external webservice tasks are done ");
        return result;
    }
    
    /**
     * use only a single external terminology
     * @param search_string
     * @param match_type
     * @param terminology
     * @return
     */
    public List<JsonObject> search(final String search_string,final  String match_type, final String terminology){
        ArrayList<String> singleTerminology = new ArrayList<>();
        singleTerminology.add(terminology.toUpperCase());
        return search(search_string, match_type, singleTerminology);
    }
}

