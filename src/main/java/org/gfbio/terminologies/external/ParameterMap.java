package org.gfbio.terminologies.external;

import java.util.HashMap;

import javax.ws.rs.WebApplicationException;

public class ParameterMap extends HashMap<String, String>{
    public ParameterMap(){
        super();
    }
    public ParameterMap(String s) throws NullPointerException{
        super();
        
        if (s.length() != 0){
        	if (!s.contains(",")){
        		String[] pair = s.split(":");
            	String attribute = pair[0].trim().substring(2, pair[0].length()-1);
            	String value = pair[1].trim().substring(1, pair[1].length()-2);
            	put(attribute, value);
        	}
        else{
	        for (String v : s.split(",")) {
	            try {
	            	String[] pair = v.split(":");
	            	String attribute = pair[0].trim().substring(2, pair[0].length()-2);
	            	String value = pair[1].trim().substring(2, pair[1].length()-2);
	            	put(attribute, value);
	            } catch (Exception ex) {
	                ex.printStackTrace();
	                throw new WebApplicationException(400);
	            }
	        }
	        }
        }
        
    }
}
