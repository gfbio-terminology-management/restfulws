package org.gfbio.terminologies.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
/**
 * @author Vincent Bohlen
 */
public class GFBioServiceUnavailableException extends WebApplicationException {

    /**
      * Create a HTTP 503 (Service Unavailable) exception.
     */
     public GFBioServiceUnavailableException() {
         super(Response.status(Status.SERVICE_UNAVAILABLE).build());
     }

     /**
      * Create a HTTP 503 (Service Unavailable) exception with text.
      * @param message the String that is the entity of the 503 response.
      */
     public GFBioServiceUnavailableException(String message) {
         super(Response.status(Status.SERVICE_UNAVAILABLE).entity(message).type("text/plain").build());
     }

}