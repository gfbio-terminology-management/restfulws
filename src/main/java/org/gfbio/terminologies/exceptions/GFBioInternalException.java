package org.gfbio.terminologies.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * @author Vincent Bohlen
 */
public class GFBioInternalException extends WebApplicationException {

    /**
      * Create a HTTP 500 (Service Unavailable) exception.
     */
     public GFBioInternalException() {
         super(Response.status(Status.INTERNAL_SERVER_ERROR).build());
     }

     /**
      * Create a HTTP 500 (Service Unavailable) exception with text.
      * @param message the String that is the entity of the 500 response.
      */
     public GFBioInternalException(String message) {
         super(Response.status(Status.INTERNAL_SERVER_ERROR).entity(message).type("text/plain").build());
     }

}