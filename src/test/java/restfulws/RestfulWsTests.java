package restfulws;
import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.gfbio.terminologies.environment.VirtuosoSettings;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.co.datumedge.hamcrest.json.SameJSONAs;
/**
 * These UnitTests test the local version against the current server version.
 * The Precondition is that the server code is correct regarding the current server code version. 
 * Before altering local code, all tests succeed.
 * The Postcondition is that tests involving changes in the current local version fail 
 * but all other tests suceed.
 * If that is the case we can assume that the local changes did not affect parts of the code we 
 * did not want to change.
 * After updating the server code all tests succeed. 
 * @author vincentb
 *
 */
public class RestfulWsTests {
    String filepath = "src/test/resources/";
    String webservicepath = "http://localhost:8080/GFBioRESTfulWS/terminologies/";
    String serverpath = "http://terminologies.gfbio.org/api/beta/terminologies/";
    
    @Test
    public void terminologiesTest(){
        String testcasepath = "";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoTest(){
        String testcasepath = "ENVO/";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void chebiTest(){
        String testcasepath = "CHEBI/";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void chebialltermsTest(){
        String testcasepath = "CHEBI/allterms";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoalltermsTest(){
        String testcasepath = "ENVO/allterms";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoIceMass(){
        String testcasepath = "ENVO/term?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_01000293";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void colPlatalea(){
        String testcasepath = "COL/term?uri=bd64eed0-76c9-46f1-91c3-13b64294d5ae";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoMetrics(){
        String testcasepath = "ENVO/metrics";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void chebiMetrics(){
        String testcasepath = "CHEBI/metrics";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoMetadata(){
        String testcasepath = "ENVO/metadata";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void chebiMetadata(){
        String testcasepath = "CHEBI/metadata";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void chebiSynonym(){
        String testcasepath = "CHEBI/synonyms?uri=http://purl.obolibrary.org/obo/CHEBI_78291";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoplantSearch(){
        String testcasepath = "search?query=plant&terminologies=ENVO";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchPlatalea(){
        String testcasepath = "search?query=platalea";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchIxodes(){
        String testcasepath = "search?query=ixodes";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchCarbon(){
        String testcasepath = "search?query=carbon";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchCarbonDioxide(){
        String testcasepath = "search?query=carbon%20dioxide";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchFirstHitPato(){
        String testcasepath = "search?query=plant&terminologies=PATO,ENVO&first_hit=true";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchFirstHitEnvo(){
        String testcasepath = "search?query=plant&terminologies=ENVO,PATO&first_hit=true";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchPlantInternalOnly(){
        String testcasepath = "search?query=plant&internal_only=true";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void searchIxodesExternalFirstHit(){
        String testcasepath = "search?query=ixodes&terminologies=WORMS,COL&first_hit=true";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoNarrower(){
        String testcasepath = "ENVO/narrower?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_010002930";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoallNarrower(){
        String testcasepath = "ENVO/allnarrower?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_010002930";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoBroader(){
        String testcasepath = "ENVO/broader?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_010002930";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    @Test
    public void envoallBroader(){    
        String testcasepath = "ENVO/allbroader?uri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FENVO_010002930";
        assertThat(
            getJson(webservicepath + testcasepath),
              SameJSONAs.sameJSONArrayAs(getJson(serverpath + testcasepath))
                  .allowingExtraUnexpectedFields()
                  .allowingAnyArrayOrdering());
    }
    /**
     * Helper method to get the data from the testing environment
     * @param url to the webservice call you want to test
     */
    private JSONArray getJson(String uri) {     
        URL url;
        StringBuilder content = new StringBuilder();
        try {
            url = new URL(uri);
            URLConnection conn = url.openConnection();
            BufferedReader br = new BufferedReader(
                               new InputStreamReader(conn.getInputStream()));
            String inputLine;
            while ((inputLine = br.readLine()) != null) {
               content.append(inputLine);
            }
            br.close();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }       
        return new JSONObject(content.toString()).getJSONArray("results");     
    }
}
